import os
import subprocess
import sys
from FileIO import *

CurrentDirectory = os.path.dirname(os.path.abspath(__file__))
#os.path.join()


#Convert to mp4
#ffmpeg -i input_filename.avi -c:v copy -c:a copy -y output_filename.mp4

#Resize/rescale
#ffmpeg -i Kazoo.mp4 -filter:v scale=120:-1 -c:a copy Kazoo_Small.mp4


def VideoToMP3(VideoFilename,OutputFilename):
	subprocess.call("ffmpeg -y -i \'"+str(VideoFilename)+"\' -codec:a libmp3lame -qscale:a 2 \'"+str(OutputFilename)+"\'",shell=True)
	
def BulkConvertVideosToMP3(InputDirectory,OutputDirectory,OptionalDeleteAfterConversion):
	
	ValidVideoExt = ["mp4","m4a","flv","webm"]
	
	for VideoExt in ValidVideoExt:
		
		GlobString = os.path.join(InputDirectory,"*."+VideoExt)
		
		print(GlobString)
		
		ListOfFiles = GlobAllFiles(GlobString);
	
		for FileInList in ListOfFiles:
			TempFilenameWithoutExt = RemoveEXT(ExtractEndFilename(FileInList))
			AudioFilename = os.path.join(OutputDirectory,TempFilenameWithoutExt+".mp3")
			
			VideoToMP3(FileInList,AudioFilename)
			
			if OptionalDeleteAfterConversion is True:
			
				DeleteFile(FileInList)
	
while True:
	
	BulkConvertVideosToMP3( os.path.join(CurrentDirectory,"VideoToAudioInput"), os.path.join(CurrentDirectory,"VideoToAudioOutput"),False)
	exit()
